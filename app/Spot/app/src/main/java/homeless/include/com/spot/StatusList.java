package homeless.include.com.spot;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class StatusList extends ArrayAdapter<String> {

    private final Activity context;
    private final String[] text;
    private final Integer[] imageID;

    public StatusList(Activity context, String[] text, Integer[] imageID) {
        super(context, R.layout.list_single, text);
        this.context = context;
        this.text = text;
        this.imageID = imageID;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.list_single, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

        ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(text[position]);

        imageView.setImageResource(imageID[position]);
        return rowView;
    }


}
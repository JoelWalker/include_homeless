package homeless.include.com.spot;

import android.*;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import org.json.JSONObject;

public class FormActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public static final String FORM_ACTIVITY_INTENT_KEY = "homeless.include.com.spot";

    GoogleApiClient googleApiClient;
    CheckBox shelter;
    CheckBox food;
    CheckBox clothes;
    CheckBox transportation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        googleApiClient = new GoogleApiClient.Builder(FormActivity.this)
                .addConnectionCallbacks(FormActivity.this)
                .addOnConnectionFailedListener(FormActivity.this)
                .addApi(LocationServices.API)
                .build();

        googleApiClient.connect();

        final EditText nameField = (EditText) findViewById(R.id.nameField);
        final RadioGroup sexidentity = (RadioGroup) findViewById(R.id.sexIdentityGroup);
        final EditText ageField = (EditText) findViewById(R.id.ageField);

        shelter = (CheckBox) findViewById(R.id.shelterCheckBox);
        food = (CheckBox) findViewById(R.id.foodCheckBox);
        clothes = (CheckBox) findViewById(R.id.clothesCheckBox);
        transportation = (CheckBox) findViewById(R.id.transportationCheckBox);
        
        Button sendButton = (Button) findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Sending POST Request", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                final StringBuilder checkBoxes = new StringBuilder();
                if(shelter.isChecked())
                    checkBoxes.append(shelter.getText() + "; ");
                if(food.isChecked())
                    checkBoxes.append(food.getText() + "; ");
                if(clothes.isChecked())
                    checkBoxes.append(clothes.getText() + "; ");
                if(transportation.isChecked())
                    checkBoxes.append(transportation.getText() + "; ");

                if (ActivityCompat.checkSelfPermission(FormActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(FormActivity.this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(FormActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 9999);
                    return;
                }

                Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

                String androidID = Settings.Secure.getString(getBaseContext().getContentResolver(),
                        Settings.Secure.ANDROID_ID);

                RadioButton chosenIdentity = (RadioButton) findViewById(sexidentity.getCheckedRadioButtonId());

                String[] userInfo = {
                        String.valueOf(location.getLongitude()),
                        String.valueOf(location.getLatitude()),
                        checkBoxes.toString(),
                        nameField.getText().toString(),
                        ageField.getText().toString(),
                        chosenIdentity.getText().toString(),
                        androidID
                };

                Intent intent = new Intent(FormActivity.this, StatusActivity.class);
                intent.putExtra(FORM_ACTIVITY_INTENT_KEY, userInfo);
                FormActivity.this.startActivity(intent);
            }
        });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}

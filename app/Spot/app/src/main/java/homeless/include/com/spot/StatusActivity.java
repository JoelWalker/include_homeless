package homeless.include.com.spot;

import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class StatusActivity extends AppCompatActivity {

    String responderName = null;
    String responderOrg = null;
    boolean beenSpotted = false;
    long responseETA = 0;

    ListView statusCards;
    StatusList adapter;
    String[] statusCardsText = {
            "Submitted",
            "Awaiting response...",
            "Awaiting responder ETA...",
            "Awaiting responder arrival..."
    };
    Integer[] imageId = {
            R.drawable.complete,
            R.drawable.waiting,
            R.drawable.waiting,
            R.drawable.waiting
    };

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        Bundle extras = getIntent().getExtras();
        String[] userInfo = extras.getStringArray(FormActivity.FORM_ACTIVITY_INTENT_KEY);

        statusCards = (ListView) findViewById(R.id.statusCards);
        adapter = new StatusList(StatusActivity.this, statusCardsText, imageId);
        statusCards.setAdapter(adapter);

        ContactAgency contactAgency = new ContactAgency();
        contactAgency.execute(userInfo);

        PingDispatcher pingDispatcher = new PingDispatcher();
        pingDispatcher.execute(userInfo);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Status Page", // TODO: Define a title for the content shown.
                // TODO: If you have statusCardsText page content that matches this app activity's content,
                // make sure this auto-generated statusCardsText page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://homeless.include.com.spot/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Status Page", // TODO: Define a title for the content shown.
                // TODO: If you have statusCardsText page content that matches this app activity's content,
                // make sure this auto-generated statusCardsText page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://homeless.include.com.spot/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    public class ContactAgency extends AsyncTask<String, String, String> {

        public ContactAgency() {
            //set context variables if required
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("create", "0");
                jsonObject.put("device_id", params[6]);

                JSONObject serverResponse = null;
                boolean messageIsEmpty = true;
                int numRequests = 0;
                while (messageIsEmpty) {
                    if(numRequests == 0) {
                        JSONObject jsonObject2 = new JSONObject();
                        jsonObject2.put("create", "1");
                        jsonObject2.put("coord_x", Double.valueOf(params[0]));
                        jsonObject2.put("coord_y", Double.valueOf(params[1]));
                        jsonObject2.put("detail", params[2]);
                        jsonObject2.put("name", params[3]);
                        jsonObject2.put("age", params[4]);
                        jsonObject2.put("gender", params[5]);
                        jsonObject2.put("device_id", params[6]);
                        serverResponse = HTTPInterface.request("request", jsonObject2);
                    }
                    else {
                        serverResponse = HTTPInterface.request("request", jsonObject);
                    }
                    numRequests++;

                    try {
                        responderOrg = serverResponse.getString("responder_org");
                        responderName = serverResponse.getString("responder_name");
                        responseETA = serverResponse.getLong("arrival_time");
                    } catch (JSONException exception) {
                        Thread.sleep(500);
                        continue;
                    }
                    messageIsEmpty = false;
                }

                return serverResponse.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responderOrg + "," + responderName + "," + responseETA;
        }


        @Override
        protected void onPostExecute(String result) {
                String ETA = new SimpleDateFormat("HH:mm:ss").format(new Date((long) (responseETA * 1000)));
                String currentTime = new SimpleDateFormat("HH:mm:ss").format(new Date());
                SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

                Date ETADate = new Date((long) (responseETA * 1000));
                Date currentTimeDate = new Date();
                long difference = ETADate.getTime() - currentTimeDate.getTime();

            String ETAString = String.format("%d hrs %d min, %d sec",
                    TimeUnit.MILLISECONDS.toHours(difference),
                    TimeUnit.MILLISECONDS.toMinutes(difference),
                    TimeUnit.MILLISECONDS.toSeconds(difference) -
                            TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(difference))
            );

            //Update the UI
            statusCardsText[1] = "Accepted by " + responderOrg;
            statusCardsText[2] = "Arriving in about " + ETAString;

            Integer[] imageId = {
                    R.drawable.complete,
                    R.drawable.complete,
                    R.drawable.complete,
                    R.drawable.waiting,

            };

            adapter = new StatusList(StatusActivity.this, statusCardsText, imageId);
            statusCards.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

    public class PingDispatcher extends AsyncTask<String, String, String> {

        public PingDispatcher() {
            //set context variables if required
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }


        @Override
        protected String doInBackground(String... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("coord_x", params[0]);
                jsonObject.put("coord_y", params[1]);
                jsonObject.put("detail", params[2]);
                jsonObject.put("name", params[3]);
                jsonObject.put("age", params[4]);
                jsonObject.put("gender", params[5]);
                jsonObject.put("device_id", params[6]);

                JSONObject serverResponse = null;
                boolean messageIsEmpty = true;
                while (messageIsEmpty) {
                    serverResponse = HTTPInterface.request("request", jsonObject);
                    try {
                        beenSpotted = serverResponse.getBoolean("spotted");
                        responseETA = serverResponse.getLong("arrival_time");
                    } catch (JSONException exception) {
                        Thread.sleep(500);
                        continue;
                    }

                    String ETA = new SimpleDateFormat("HH:mm:ss").format(new Date((long) (responseETA * 1000)));
                    String currentTime = new SimpleDateFormat("HH:mm:ss").format(new Date());
                    SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

                    Date ETADate = new Date((long) (responseETA * 1000));
                    Date currentTimeDate = new Date();
                    long difference = ETADate.getTime() - currentTimeDate.getTime();

                    String ETAString = String.format("%d hrs %d min, %d sec",
                            TimeUnit.MILLISECONDS.toHours(difference),
                            TimeUnit.MILLISECONDS.toMinutes(difference),
                            TimeUnit.MILLISECONDS.toSeconds(difference) -
                                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(difference))
                    );

                    statusCardsText[2] = "Arriving in about " + ETAString;

                    if(beenSpotted == false) {
                        Thread.sleep(500);
                        continue;
                    }
                    messageIsEmpty = false;
                }

                return serverResponse.toString();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return responderOrg + "," + responderName + "," + responseETA;
        }


        @Override
        protected void onPostExecute(String result) {
            // Update the UI
            statusCardsText[3] = "You've been Spotted!";

            Integer[] imageId = {
                    R.drawable.complete,
                    R.drawable.complete,
                    R.drawable.complete,
                    R.drawable.complete,

            };

            adapter = new StatusList(StatusActivity.this, statusCardsText, imageId);
            statusCards.setAdapter(adapter);
            adapter.notifyDataSetChanged();
        }
    }

}

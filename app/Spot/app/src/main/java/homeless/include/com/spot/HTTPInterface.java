package homeless.include.com.spot;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by joel on 10/21/16.
 */
public class HTTPInterface {
    public static JSONObject request(String endpoint, JSONObject post_data) throws Exception {
        URL url = make_url(endpoint);
        HttpURLConnection conn = open_connection(url);

        String data = post_data.toString();
        begin_request(conn, data);
        write_request(conn, data);
        end_request(conn, data);

        try {
            InputStream input_stream = make_input_stream(conn);
            BufferedReader reader = make_buffered_reader(input_stream);
            String response = read_everything(reader);

            int code = read_response_code(conn);
            JSONObject obj = parse_response(response);

            if (code >= 200 && code <= 299) {
                return obj;
            } else {
                throw new Exception("Server exception");
            }
        } finally {
            conn.disconnect();
        }
    }

        private static URL make_url(String endpoint) throws Exception {
        try {
            return new URL("http://globalhack-2016-include-homeless-jwalker5.c9users.io:80/" + endpoint);
        } catch (MalformedURLException e) {
            throw e;
        }
    }

    private static HttpURLConnection open_connection(URL url) throws Exception {
        try {
            return (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            throw e;
        }
    }

    private static void begin_request(HttpURLConnection conn, String data) {
        conn.setDoOutput(true);
        conn.setFixedLengthStreamingMode(data.length());
    }

    private static void write_request(HttpURLConnection conn, String data) throws Exception {
        try {
            conn.getOutputStream().write(data.getBytes());
        } catch (IOException e) {
            throw e;
        }
    }

    private static void end_request(HttpURLConnection conn, String data) throws Exception {
        try {
            conn.getOutputStream().close();
        } catch (IOException e) {
            throw e;
        }
    }

    private static InputStream make_input_stream(HttpURLConnection conn) throws Exception {
        try {
            return new BufferedInputStream(conn.getInputStream());
        } catch (IOException e) {
            throw e;
        }
    }

    private static BufferedReader make_buffered_reader(InputStream input_stream) throws Exception {
        try {
            return new BufferedReader(new InputStreamReader(input_stream, "UTF-8"), 8);
        } catch (UnsupportedEncodingException e) {
            throw e;
        }
    }

    private static String read_everything(BufferedReader reader) throws Exception {
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null)
            {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            throw e;
        }

        return sb.toString();
    }

    private static int read_response_code(HttpURLConnection conn) throws Exception {
        try {
            return conn.getResponseCode();
        } catch (IOException e) {
            throw e;
        }
    }

    private static JSONObject parse_response(String response) throws Exception {
        try {
            return new JSONObject(response);
        } catch (JSONException e) {
            throw e;
        }
    }
}


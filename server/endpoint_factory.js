let config = require('./config');
let make_sequence = require('./make_sequence');


module.exports.make_http_endpoint = function(action_seq, pg_pool, extra) {
    return make_sequence([
        function(request, data, response) {
            this.extra = extra;

            this.request = request;
            this.data = data;
            this.response = response;

            if (pg_pool) {
                pg_pool.connect(this.seq.next);
            } else {
                this.seq.jump('start').next();
            }
        },
        function(err, client, done) {
            if (err) {
                return this.seq.jump('error').next(500, 'Error connecting to pg: ' + err);
            }

            this.client = client;
            this.done = done;

            this.seq.jump('start').next();
        },
        'success', function(data) {
            if (typeof this.done === 'function') {
                this.done();
            }
            this.response.write_success(data);
        },
        'error', function(code, msg) {
            if (typeof this.done === 'function') {
                this.done();
            }
            this.response.write_error(code, msg);
        },
        'start', action_seq,
    ]);
};


module.exports.make_internal_endpoint = function(action_seq, pg_pool, extra) {
    return make_sequence([
        function() {
            this.extra = extra;

            if (pg_pool) {
                pg_pool.connect(this.seq.next);
            } else {
                this.seq.jump('start').next();
            }
        },
        function(err, client, done) {
            if (err) {
                return this.seq.jump('error').next(500, 'Error connecting to pg: ' + err);
            }

            this.client = client;
            this.done = done;

            this.seq.jump('start').next();
        },
        'success', function() {
            if (typeof this.done === 'function') {
                this.done();
            }
        },
        'error', function(code, msg) {
            if (typeof this.done === 'function') {
                this.done();
            }
            console.error(code, msg);
        },
        'start', action_seq,
    ]);
};

let fs = require('fs');

const PROD = false;

module.exports = {
    'pg': {
        'host': 'localhost',
        'user': 'server', //env var: PGUSER
        'database': 'include_homeless', //env var: PGDATABASE
        'password': '9fee79b7f3172c1b20d35097b6bd5eb1', //env var: PGPASSWORD
        'port': 5432, //env var: PGPORT
        'max': 10, // max number of clients in the pool
        'idleTimeoutMillis': 30000, // how long a client is allowed to remain idle before being closed
    },
    'https': {
        'key': fs.readFileSync(__dirname + '/cert/key.pem'),
        'cert': fs.readFileSync(__dirname + '/cert/key-cert.pem'),
    },
    'use_https': false,
    'log_io': true,
    'bcrypt_rounds': 8,
    'server_http_port': process.env.PORT || 8082,
    'server_http_ip': process.env.IP || undefined,
    'google_maps_api_key': 'AIzaSyCqKMztvH0KS2SJI0Et0wRFINHBCFyKKT0',
};

if (PROD) {
    let eq = function(a, b) {return a === b;};
    let neq = function(a, b) {return a !== b;};
    let gte = function(a, b) {return a >= b;};
    let lte = function(a, b) {return a <= b;};

    let check_prop = function(key, cmp, threshold) {
        let val = module.exports[key];
        if (!cmp(val, threshold)) {
            throw new Error('Invalid PROD config property for ' + key);
        }
    };

    check_prop('use_https', eq, true);
    check_prop('log_io', eq, false);
}

let flatten = function(arr) {
    return arr.reduce(function(flat, el) {
        return flat.concat(Array.isArray(el) ? flatten(el) : el);
    }, []);
};

module.exports = function(funcs) {
    let labels = {};
    let res_index = 0;
    funcs = flatten(funcs).filter(function(val) {
        if (typeof val === 'string') {
            labels[val] = res_index;
        }

        let is_func = typeof val === 'function';
        res_index += is_func;
        return is_func;
    });

    return function(name) {
        console.log('Starting...');

        let cur = 0;

        let obj = {
            'seq': {
                'next': function() {
                    console.log('func[' + cur + ']');

                    funcs[cur++].apply(obj, arguments);
                },
                'jump': function(label) {
                    if (typeof labels[label] === 'number') {
                        cur = labels[label];
                    } else {
                        console.error('Invalid label ' + label);
                    }
                    return obj.seq;
                },
            },
        };

        obj.seq.next.apply(null, arguments);
    };
};

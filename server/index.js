let pg = require('pg');

let config = require('./config');
let endpoint_factory = require('./endpoint_factory');


let pg_pool = new pg.Pool(config.pg);
let http_server;


let setup_http = function() {
    let actions = {};
    actions['/config'] = endpoint_factory.make_http_endpoint(require('./actions/config'), null);
    actions['/register'] = endpoint_factory.make_http_endpoint(require('./actions/register'), pg_pool);
    actions['/update_shelter'] = endpoint_factory.make_http_endpoint(require('./actions/update_shelter'), pg_pool);
    actions['/request'] = endpoint_factory.make_http_endpoint(require('./actions/request'), pg_pool);
    actions['/list'] = endpoint_factory.make_http_endpoint(require('./actions/list'), pg_pool);
    actions['/accept'] = endpoint_factory.make_http_endpoint(require('./actions/accept'), pg_pool);
    actions['/arrive'] = endpoint_factory.make_http_endpoint(require('./actions/arrive'), pg_pool);
    actions['/volunteer_update'] = endpoint_factory.make_http_endpoint(require('./actions/volunteer_update'), pg_pool);

    if (config.use_https) {
        let https = require('https');
        http_server = https.createServer(config.https);
    } else {
        let http = require('http');
        http_server = http.createServer();
    }

    let http_request_listener = require('./http_request_listener')(actions);
    http_server.on('request', http_request_listener);

    http_server.listen(config.server_http_port);
};


let setup_crons = function() {
    /*
    let action_udp_download = endpoint_factory.make_internal_endpoint(require('./actions/udp_download'), pg_pool, {
        'udp_server': udp_server,
    });

    //setInterval(action_udp_download, config.udp_download_query_interval);
    */
};


setup_http();
setup_crons();

#!/bin/sh

openssl genrsa 2048 > key.pem
openssl req -x509 -new -key key.pem > key-cert.pem


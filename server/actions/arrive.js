let config = require('../config');
let validators = require('../validators');

let time_estimator = require('./time_estimator');

module.exports = [
    require('./login'),
    function() {
        if (!validators.is_id(this.data.request_id)) {
            return this.seq.jump('error').next(400, 'Must provide request_id property');
        }

        let sql = 'UPDATE requests\
            SET arrival_time = NOW()\
            WHERE id = $1';
        this.client.query(sql, [this.data.request_id], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error updating request as arrived: ' + err);
        }

        if (result.rowCount === 0) {
            return this.seq.jump('error').next(400, 'Request does not exist');
        }

        return this.seq.jump('success').next({});
    },
];

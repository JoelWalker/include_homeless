let config = require('../config');
let validators = require('../validators');

module.exports = [
    require('./login'),
    function() {
        let sql = 'SELECT name FROM shelters WHERE id = $1';
        this.client.query(sql, [this.volunteer.shelter_id], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error selecting shelter: ' + err);
        }

        this.shelter_name = result.rows[0].name;

        let sql = 'SELECT\
            requests.id AS id,\
            requests.send_time AS send_time,\
            requests.detail AS detail,\
            requests.gender AS request_gender,\
            requests.age AS request_age,\
            requests.name AS request_name,\
            responder.name AS responder_name,\
            shelters.name AS responder_org,\
            requests.response_time AS response_time,\
            requests.arrival_time AS arrival_time,\
            ST_Distance(requests.location, me.location) AS my_distance,\
            ST_Distance(requests.location, responder.location) AS responder_distance,\
            ST_X(requests.location::geometry) AS request_x,\
            ST_Y(requests.location::geometry) AS request_y\
            FROM requests\
            LEFT JOIN volunteers AS responder ON responder.id = requests.responder_id\
            INNER JOIN volunteers AS me ON me.id = $1\
            LEFT JOIN shelters ON shelters.id = responder.shelter_id\
            WHERE (responder_id ' + (this.data.tab === 'new' ? 'IS NULL OR response_time > NOW() - interval \'10 seconds\'' : (this.data.tab === 'accepted' ? 'IS NOT NULL AND (arrival_time IS NULL OR arrival_time > NOW() - interval \'10 seconds\')' : 'IS NOT NULL AND arrival_time IS NOT NULL')) + ')\
            ORDER BY ' + (this.data.tab === 'new' ? 'requests.send_time ASC' : (this.data.tab === 'accepted' ? 'requests.responder_id = $1 DESC, ' : '') + 'requests.response_time DESC') + '\
            LIMIT 10';
        this.client.query(sql, [this.volunteer.id], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error selecting requests: ' + err);
        }

        return this.seq.jump('success').next({
            'shelter_name': this.shelter_name,
            'requests': result.rows,
        });
    },
];

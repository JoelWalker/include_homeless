var username;
var password;

var tab = 'new';
var show_male = true;
var show_female = true;

$('.login_button').on('click', function() {
    username = $('.username').val();
    password = $('.password').val();

    update_requests(function() {
        $('.login_screen').hide();
        $('.list_screen').show();

        setInterval(update_requests, 2000);

        var watch_id = navigator.geolocation.watchPosition(function(pos) {
            var request = $.ajax({
                'url': '/volunteer_update',
                'method': 'POST',
                'dataType': 'json',
                'data': JSON.stringify({
                    'username': username,
                    'pass': password,
                    'coord_x': pos.coords.longitude,
                    'coord_y': pos.coords.latitude,
                }),
            });

            request.done(function(msg) {
                if (msg.error) {
                    alert(msg.error);
                }
            });

            request.fail(function(jqXHR, textStatus) {
                alert('Request failed: ' + textStatus);
            });
        }, function() {}, {
            'enableHighAccuracy': true
        });
    }, function(msg) {
        if (msg === 'username_not_found') {
            $('.username').addClass('invalid')
            setTimeout(function() {
                $('.username').removeClass('invalid');
            }, 1000);
        } else if (msg === 'invalid_password') {
            $('.password').addClass('invalid')
            setTimeout(function() {
                $('.password').removeClass('invalid');
            }, 1000);
        }
    });
});

$('.tab_new').on('click', function() {
    $('.tab_new').addClass('tab_selected');
    $('.tab_accepted').removeClass('tab_selected');
    $('.tab_completed').removeClass('tab_selected');

    tab = 'new';
    update_requests();
});
$('.tab_accepted').on('click', function() {
    $('.tab_new').removeClass('tab_selected');
    $('.tab_accepted').addClass('tab_selected');
    $('.tab_completed').removeClass('tab_selected');

    tab = 'accepted';
    update_requests();
});
$('.tab_completed').on('click', function() {
    $('.tab_new').removeClass('tab_selected');
    $('.tab_accepted').removeClass('tab_selected');
    $('.tab_completed').addClass('tab_selected');

    tab = 'completed';
    update_requests();
});

$('.show_male, .show_female').on('change', function() {
    show_male = $('.show_male').is(':checked');
    show_female = $('.show_female').is(':checked');
    update_filters();
});

$('.login_expand').on('click', function() {
    $('.menu').toggleClass('open');
});

$('.logout').on('click', function() {
    document.location.href = '/';
});

setTimeout(function() {
    $('.login_screen').addClass('with_login');
}, 3000);


var $requests_list = $('.requests_list');
var update_requests = function(succ, fail) {
    if (typeof succ !== 'function') {succ = function() {};}
    if (typeof fail !== 'function') {fail = alert;}

    var request = $.ajax({
        'url': '/list',
        'method': 'POST',
        'dataType': 'json',
        'data': JSON.stringify({
            'username': username,
            'pass': password,
            'tab': tab,
        }),
    });

    request.done(function(msg) {
        if (msg.error) {
            fail(msg.error);
            return;
        }

        $('.header_title').text(msg.shelter_name);

        $requests_list.empty();
        msg.requests.forEach(function(request) {
            var html = '';

            html += '<div class="request ' + (request.request_gender ? 'gender_' + request.request_gender : '') + '">';
                if (!request.request_name) {request.request_name = 'Name Unknown';}
                html += '<span class="request_name">' + request.request_name + '</span>';
                if (request.send_time) {html += '<span class="request_time"><i class="fa fa-clock-o" aria-hidden="true"></i> <time class="timeago" datetime="' + request.send_time + '">' + request.send_time + '</time></span>';}
                var details = [request.request_gender, request.request_age, request.detail].filter(Boolean);
                if (details) {html += '<span class="request_detail">' + details.join('<br />') + '</span>';}
                if (typeof request.my_distance === 'number') {html += '<span class="request_distance">' + (Math.round(request.my_distance * 0.000621371 * 10) / 10) + 'mi -  <a href="https://maps.google.com/?q=' + request.request_y + ',' + request.request_x + '" target="_blank">maps.google.com</a></span>';}
                if (tab === 'new') {html += '<button class="request_accept" ' + (request.response_time ? 'disabled="disabled"' : '') + '>Accept</button>';}
                if (tab === 'accepted') {html += '<button class="request_arrive" ' + (request.arrival_time ? 'disabled="disabled"' : '') + '>Spot</button>';}
                html += '<span class="request_responder">' + (request.response_time ? (request.arrival_time ? 'completed by ' : 'accepted by ') + request.responder_name + ' from ' + request.responder_org : '') + '</span>';
            html += '</div>';

            var $el = $(html);
            $requests_list.append($el);

            $el.find('time.timeago').timeago();
            $el.find('.request_accept').on('click', function() {
                $(this).attr('disabled', 'disabled');
                hit_request('/accept', request.id);
            });
            $el.find('.request_arrive').on('click', function() {
                $(this).attr('disabled', 'disabled');
                hit_request('/arrive', request.id);
            });
        });

        update_filters();

        succ();
    });

    request.fail(function(jqXHR, textStatus) {
        fail('Request failed: ' + textStatus);
    });
};

var update_filters = function() {
    $('.gender_male')[show_male ? 'show' : 'hide']();
    $('.gender_female')[show_female ? 'show' : 'hide']();
};

var hit_request = function(endpoint, id) {
    var request = $.ajax({
        'url': endpoint,
        'method': 'POST',
        'dataType': 'json',
        'data': JSON.stringify({
            'username': username,
            'pass': password,
            'request_id': parseInt(id),
        }),
    });

    request.done(function(msg) {
        if (msg.error) {
            alert(msg.error);
            return;
        }

        update_requests();
    });

    request.fail(function(jqXHR, textStatus) {
        alert('Request failed: ' + textStatus);
    });
};

/*
$('.username').val('joel');
$('.password').val('pass');
$('.login_button').trigger('click');
*/

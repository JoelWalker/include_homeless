let crypto = require('crypto');
let bcrypt = require('bcryptjs');

let config = require('../config');
let validators = require('../validators');

module.exports = [
    function() {
        if (!validators.is_string(this.data.user)) {
            return this.seq.jump('error').next(400, 'Must provide user property');
        }
        if (!validators.is_string(this.data.pass)) {
            return this.seq.jump('error').next(400, 'Must provide pass property');
        }
        if (!validators.is_id(this.data.shelter_id)) {
            return this.seq.jump('error').next(400, 'Must provide shelter_id property');
        }

        let username = this.data.user.toLowerCase();
        let sql = 'SELECT id, username, pass FROM admins WHERE username = $1 LIMIT 1';
        this.client.query(sql, [username], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error querying user: ' + err);
        }
        if (!result.rowCount) {
            return this.seq.jump('success').next({
                'error': 'username_not_found',
            });
        }

        this.admin_id = parseInt(result.rows[0].id);
        this.username = result.rows[0].username;
        let pass = result.rows[0].pass.toString();
        bcrypt.compare(this.data.pass, pass, this.seq.next);
    },
    function(err, same) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error comparing password: ' + err);
        }
        if (!same) {
            return this.seq.jump('success').next({
                'error': 'invalid_password',
            });
        }

        let sets = [];
        let params = [this.data.shelter_id, this.admin_id];

        if (typeof this.data.name === 'string') {
            sets.push('name = $' + (params.length + 1));
            params.push(this.data.name);
        }

        if (typeof this.data.coord_x === 'number' && typeof this.data.coord_y === 'number') {
            sets.push('location = ST_MakePoint($' + (params.length + 1) + '::float, $' + (params.length + 2) + '::float)');
            params.push(this.data.coord_x);
            params.push(this.data.coord_y);
        }

        if (sets.length === 0) {
            return this.seq.jump('error').next(400, 'Please specify at least one parameter');
        }

        let sql = 'UPDATE shelters SET ' + sets.join(', ') + ' WHERE id = $1 AND admin_id = $2';
        this.client.query(sql, params, this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error updating shelter: ' + err);
        }

        if (!result.rowCount) {
            return this.seq.jump('error').next(400, 'You don\'t have access to that shelter');
        }

        return this.seq.jump('success').next({});
    },
];

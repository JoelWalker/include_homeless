let bcrypt = require('bcryptjs');

let config = require('../config');
let validators = require('../validators');

module.exports = [
    function() {
        if (!validators.is_string(this.data.username)) {
            return this.seq.jump('error').next(400, 'Must provide username property');
        }
        if (!validators.is_string(this.data.pass)) {
            return this.seq.jump('error').next(400, 'Must provide pass property');
        }

        let username = this.data.username.toLowerCase();
        let sql = 'SELECT * FROM volunteers WHERE username = $1 LIMIT 1';
        this.client.query(sql, [username], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error querying user: ' + err);
        }
        if (result.rowCount === 0) {
            return this.seq.jump('success').next({
                'error': 'username_not_found',
            });
        }

        this.volunteer = result.rows[0];
        let pass = this.volunteer.pass.toString();
        bcrypt.compare(this.data.pass, pass, this.seq.next);
    },
    function(err, same) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error comparing password: ' + err);
        }
        if (!same) {
            return this.seq.jump('success').next({
                'error': 'invalid_password',
            });
        }

        this.seq.next();
    },
];

let config = require('../config');
let validators = require('../validators');

let time_estimator = require('./time_estimator');

module.exports = [
    require('./login'),
    function() {
        if (!validators.is_id(this.data.request_id)) {
            return this.seq.jump('error').next(400, 'Must provide request_id property');
        }

        let sql = 'UPDATE requests\
            SET responder_id = $2,\
                response_time = NOW(),\
                response_message = $3\
            WHERE id = $1';
        this.client.query(sql, [this.data.request_id, this.volunteer.id, null], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error updating request with responder: ' + err);
        }

        if (result.rowCount === 0) {
            return this.seq.jump('error').next(400, 'Request does not exist');
        }

        return this.seq.jump('success').next({});
    },
];

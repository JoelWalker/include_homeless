var bcrypt = require('bcryptjs');

let config = require('../config');
let validators = require('../validators');

module.exports = [
    function() {
        if (!validators.is_string(this.data.username)) {
            return this.seq.jump('error').next(400, 'Must provide username property');
        }
        if (!validators.is_string(this.data.pass)) {
            return this.seq.jump('error').next(400, 'Must provide pass property');
        }
        if (!validators.is_string(this.data.name)) {
            return this.seq.jump('error').next(400, 'Must provide name property');
        }
        if (!validators.is_id(this.data.shelter_id)) {
            return this.seq.jump('error').next(400, 'Must provide shelter_id property');
        }

        let sql = 'SELECT id FROM volunteers\
            WHERE username = $1\
            LIMIT 1';
        this.client.query(sql, [this.data.username.toLowerCase()], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error querying user: ' + err);
        }
        if (result.rowCount) {
            return this.seq.jump('success').next({
                'error': 'user_already_exists',
            });
        }

        bcrypt.hash(this.data.pass, config.bcrypt_rounds, this.seq.next);
    },
    function(err, hash) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error generating hash: ' + err);
        }

        let sql = 'INSERT INTO volunteers (username, pass, name, shelter_id, create_time)\
            VALUES ($1, $2, $3, $4, NOW())';
        this.client.query(sql, [this.data.username.toLowerCase(), hash, this.data.name, this.data.shelter_id], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error creating user: ' + err);
        }

        return this.seq.jump('success').next({});
    },
];

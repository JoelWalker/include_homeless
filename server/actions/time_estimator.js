let config = require('../config');

let maps_client = require('@google/maps').createClient({
    'key': config.google_maps_api_key
});

module.exports = function(c1_x, c1_y, c2_x, c2_y, callback) {
    maps_client.distanceMatrix({
        'origins': c1_x + ',' + c1_y,
        'destinations': c2_x + ',' + c2_y,
    }, function(err, resp) {
        if (err) {
            callback(err);
        } else if (
            resp.json
            && resp.json.rows
            && resp.json.rows[0]
            && resp.json.rows[0].elements
            && resp.json.rows[0].elements[0]
            && resp.json.rows[0].elements[0].duration
            && typeof resp.json.rows[0].elements[0].duration.value === 'number'
        ) {
            console.log(resp.json.rows[0].elements[0]);
            console.log(resp.json.origin_addresses[0]);
            console.log(resp.json.destination_addresses[0]);
            callback(undefined, resp.json.rows[0].elements[0].duration.value);
        } else {
            callback('invalid google maps api response: ' + JSON.stringify(resp));
        }
    })
};

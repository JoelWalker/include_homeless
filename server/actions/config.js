let config = require('../config');

module.exports = [
    function() {
        return this.seq.jump('success').next({
            'use_https': config.use_https,
        });
    },
];

let config = require('../config');
let validators = require('../validators');

let time_estimator = require('./time_estimator');

module.exports = [
    function() {
        if (!validators.is_string(this.data.device_id)) {
            return this.seq.jump('error').next(400, 'Must provide device_id property');
        }

        this.device_id = this.data.device_id.toLowerCase();
        let sql = 'UPDATE homeless_devices SET last_conn_time = NOW() WHERE device_id = $1 RETURNING id';
        this.client.query(sql, [this.device_id], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error updating homeless device: ' + err);
        }

        if (result.rowCount !== 0) {
            this.homeless_device_id = result.rows[0].id;
            return this.seq.jump('have_device_id').next();
        }

        let sql = 'INSERT INTO homeless_devices (device_id, last_conn_time) VALUES ($1, NOW()) RETURNING id';
        this.client.query(sql, [this.device_id], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error inserting homeless device: ' + err);
        }

        this.homeless_device_id = result.rows[0].id;
        this.seq.jump('have_device_id').next();
    },
    'have_device_id', function() {
        if (parseInt(this.data.create)) {
            return this.seq.jump('create').next();
        } else {
            return this.seq.jump('update').next();
        }
    },
    'create', function() {
        let sql = 'SELECT COUNT(*) AS count FROM requests WHERE homeless_device_id = $1 AND arrival_time IS NULL';
        this.client.query(sql, [this.homeless_device_id], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error counting requests: ' + err);
        }

        if (parseInt(result.rows[0].count)) {
            return this.seq.jump('success').next({});
        }

        let cols = ['send_time', 'homeless_device_id'];
        let vals = ['NOW()', '$1'];
        let params = [this.homeless_device_id];

        if (typeof this.data.coord_x !== 'undefined' && typeof this.data.coord_y !== 'undefined') {
            cols.push('location');
            vals.push('ST_GeographyFromText(\'SRID=4326;POINT(\' || $' + (params.length + 1) + '::float || \' \' || $' + (params.length + 2) + '::float || \')\')');
            params.push(parseFloat(this.data.coord_x));
            params.push(parseFloat(this.data.coord_y));
        }

        if (typeof this.data.detail === 'string') {
            cols.push('detail');
            vals.push('$' + (params.length + 1));
            params.push(this.data.detail);
        }

        if (typeof this.data.gender === 'string') {
            cols.push('gender');
            vals.push('$' + (params.length + 1));
            params.push(this.data.gender.toLowerCase());
        }

        if (typeof this.data.age !== 'undefined') {
            cols.push('age');
            vals.push('$' + (params.length + 1));
            params.push(parseInt(this.data.age));
        }

        if (typeof this.data.name === 'string') {
            cols.push('name');
            vals.push('$' + (params.length + 1));
            params.push(this.data.name);
        }

        let sql = 'INSERT INTO requests (' + cols.join(', ') + ') VALUES (' + vals.join(', ') + ')';
        this.client.query(sql, params, this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error inserting request: ' + err);
        }

        this.seq.jump('success').next({});
    },
    'update', function() {
        let sql = 'SELECT id FROM requests WHERE homeless_device_id = $1 ORDER BY send_time DESC LIMIT 1';
        this.client.query(sql, [this.homeless_device_id], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error inserting request: ' + err);
        }

        if (result.rowCount === 0) {
            return this.seq.jump('error').next(400, 'Request does not exist');
        }

        let sets = [];
        let params = [result.rows[0].id];

        if (typeof this.data.coord_x !== 'undefined' && typeof this.data.coord_y !== 'undefined') {
            sets.push('location = ST_GeographyFromText(\'SRID=4326;POINT(\' || $' + (params.length + 1) + '::float || \' \' || $' + (params.length + 2) + '::float || \')\')');
            params.push(parseFloat(this.data.coord_x));
            params.push(parseFloat(this.data.coord_y));
        }

        if (typeof this.data.detail === 'string') {
            sets.push('detail = $' + (params.length + 1));
            params.push(this.data.detail);
        }

        if (typeof this.data.gender === 'string') {
            sets.push('gender = $' + (params.length + 1));
            params.push(this.data.gender.toLowerCase());
        }

        if (typeof this.data.age !== 'undefined') {
            sets.push('age = $' + (params.length + 1));
            params.push(parseInt(this.data.age));
        }

        if (typeof this.data.name === 'string') {
            sets.push('name = $' + (params.length + 1));
            params.push(this.data.name);
        }

        if (sets.length === 0) {
            sets.push('name = name');
        }

        let sql = 'UPDATE requests SET ' + sets.join(', ') + ' WHERE id = $1 RETURNING responder_id, ST_X(location::geometry) AS request_x, ST_Y(location::geometry) AS request_y, arrival_time IS NOT NULL AS arrival_time';
        this.client.query(sql, params, this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error updating request: ' + err);
        }

        if (result.rowCount !== 0 && result.rows.length) {
            this.responder_id = result.rows[0].responder_id;
            this.request_x = result.rows[0].request_x;
            this.request_y = result.rows[0].request_y;
            this.arrival_time = !!result.rows[0].arrival_time;

            if (this.responder_id) {
                return this.seq.jump('return_responder').next();
            }
        }

        return this.seq.jump('success').next({});
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error inserting/updating request: ' + err);
        }

        return this.seq.jump('success').next({});
    },
    'return_responder', function() {
        let sql = 'SELECT\
            volunteers.name AS responder_name,\
            ST_X(volunteers.location::geometry) AS responder_x,\
            ST_Y(volunteers.location::geometry) AS responder_y,\
            shelters.name AS shelter_name\
            FROM volunteers\
            INNER JOIN shelters ON shelters.id = volunteers.shelter_id\
            WHERE volunteers.id = $1';
        this.client.query(sql, [this.responder_id], this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error querying responder: ' + err);
        }

        if (result.rowCount === 0) {
            return this.seq.jump('error').next(500, 'Error querying responder - no results: ' + err);
        }

        let row = result.rows[0];
        this.responder_name = row.responder_name;
        this.shelter_name = row.shelter_name;

        time_estimator(row.responder_y, row.responder_x, this.request_y, this.request_x, this.seq.next);
    },
    function(err, time) {
        if (err) {
            console.error('Error predicting up arrival_time: ' + err);
            time = 15 * 60;
        }

        return this.seq.jump('success').next({
            'responder_name': this.responder_name,
            'responder_org': this.shelter_name,
            'arrival_time': Math.round(Date.now() / 1000 + time),
            'spotted': this.arrival_time,
            //'message': this.responder_message,
        });
    },
];

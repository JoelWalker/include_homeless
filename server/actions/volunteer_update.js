let config = require('../config');
let validators = require('../validators');

module.exports = [
    require('./login'),
    function() {
        let sets = [];
        let params = [this.volunteer.id];

        if (typeof this.data.new_username === 'string') {
            sets.push('username = $' + (params.length + 1));
            params.push(this.data.new_username.toLowerCase());
        }

        if (typeof this.data.name === 'string') {
            sets.push('name = $' + (params.length + 1));
            params.push(this.data.name);
        }

        if (typeof this.data.email === 'string') {
            sets.push('email = $' + (params.length + 1));
            params.push(this.data.email);
        }

        if (typeof this.data.image === 'string') {
            sets.push('image = $' + (params.length + 1));
            params.push(this.data.image);
        }

        if (typeof this.data.shelter_id === 'number') {
            sets.push('shelter_id = $' + (params.length + 1));
            params.push(this.data.shelter_id);
        }

        if (typeof this.data.coord_x === 'number' && typeof this.data.coord_y === 'number') {
            sets.push('location = ST_SetSRID(ST_MakePoint($' + (params.length + 1) + '::float, $' + (params.length + 2) + '::float), 4326)');
            params.push(this.data.coord_x);
            params.push(this.data.coord_y);
        }

        if (sets.length === 0) {
            return this.seq.jump('error').next(400, 'Please specify at least one property to set');
        }

        let sql = 'UPDATE volunteers SET ' + sets.join(', ') + ' WHERE id = $1';
        this.client.query(sql, params, this.seq.next);
    },
    function(err, result) {
        if (err) {
            return this.seq.jump('error').next(500, 'Error updating request: ' + err);
        }

        // TODO: update requests arrival time

        return this.seq.jump('success').next({});
    },
];

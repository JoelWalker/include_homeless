DROP TABLE IF EXISTS requests;
DROP TABLE IF EXISTS homeless_devices;
DROP TABLE IF EXISTS volunteers;
DROP TABLE IF EXISTS shelters;

-- To install postgis:
-- CREATE EXTENSION postgis;
-- ALTER EXTENSION postgis UPDATE TO "2.3.0";

CREATE TABLE shelters (
    id BIGSERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    location GEOGRAPHY(POINT, 4326)
) WITH (OIDS = FALSE);

CREATE TABLE volunteers (
    id BIGSERIAL PRIMARY KEY,
    username TEXT NOT NULL,
    pass BYTEA NOT NULL,
    name TEXT NOT NULL,
    email TEXT NULL,
    image BYTEA NULL,
    shelter_id BIGINT NOT NULL REFERENCES shelters,
    create_time TIMESTAMP NOT NULL,
    location GEOGRAPHY(POINT, 4326) NULL
) WITH (OIDS = FALSE);

CREATE TABLE homeless_devices (
    id BIGSERIAL PRIMARY KEY,
    device_id TEXT NOT NULL,
    last_conn_time TIMESTAMP NOT NULL
) WITH (OIDS = FALSE);

CREATE TABLE requests (
    id BIGSERIAL PRIMARY KEY,
    send_time TIMESTAMP NOT NULL,
    homeless_device_id BIGINT NOT NULL REFERENCES homeless_devices,
    location GEOGRAPHY(POINT, 4326) NULL,
    detail TEXT NULL,
    gender TEXT NULL,
    age BIGINT NULL,
    name TEXT NULL,
    responder_id BIGINT NULL REFERENCES volunteers,
    response_time TIMESTAMP NULL,
    response_message TEXT NULL,
    arrival_time TIMESTAMP NULL
) WITH (OIDS = FALSE);

CREATE USER server WITH password '9fee79b7f3172c1b20d35097b6bd5eb1';

GRANT SELECT ON shelters, volunteers, homeless_devices, requests TO server;
GRANT INSERT ON shelters, volunteers, homeless_devices, requests TO server;
GRANT UPDATE ON shelters, volunteers, homeless_devices, requests TO server;
GRANT DELETE ON shelters, volunteers, homeless_devices, requests TO server;
GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO server;

let config = require('./config');

module.exports.is_int = function(val) {return typeof val === 'number' && val % 1 === 0;};
module.exports.is_uint = function(val) {return typeof val === 'number' && val % 1 === 0 && val >= 0;};
module.exports.is_id = module.exports.is_uint;
module.exports.is_string = function(val) {return typeof val === 'string';};
module.exports.is_secret = function(val) {return typeof val === 'string';};
module.exports.is_float = function(val) {return typeof val === 'number';};
